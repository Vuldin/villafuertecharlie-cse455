package com.example.ozmo.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;

public class MainActivity extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    String url = "https://api.fixer.io/latest?base=USD"; //json format
    String json = "";

    String line = "";
    // rate will receive exchange info
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);


        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                System.out.println("\n test before async \n");
                BackgroundTask object = new BackgroundTask();
                object.execute();
                System.out.println("\n test after async \n");
                //textView01.setText("TEST!");
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String res) {
            textView01.setText("$" + usd + " = " + "¥" + res);
            editText01.setText("");
            super.onPostExecute(res);

        }

        @Override
        protected String doInBackground(Void... params) {
            // bulk of asynch task

            try {
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESFUL");

                while (line != null) {
                    line = bufferedReader.readLine();
                    json += line;
                }
                System.out.println("\nTHE JSN: " + json);

                JSONObject obj = new JSONObject(json); //create json object
                JSONObject objRate = obj.getJSONObject("rates"); //creat second json object to get rates
                rate = objRate.get("JPY").toString(); // gets jpy and sets to string
                System.out.println("\n What is rate: " + rate + "\n");

                Double value = Double.parseDouble(rate);
                /** currecny conversion logic below**/
                //covert user's input to string
                usd = editText01.getText().toString();
                //if-else statement to make sure user cannot leave the EditText blank
                if (usd.equals("")) {
                    textView01.setText("This field cannot be blank!");
                } else {
                    //Convert string to double
                    Double dInputs = Double.parseDouble(usd);
                    //convert string to double

                    //Convert function
                    Double result = dInputs * value;
                    //Display the result
                    String res = Double.toString(result);
                    //onPostExecute(res);
                    System.out.println("\n$" + usd + " = " + "¥" + String.format("%.2f", result));
                    return res;


                    //clear the edittext after clicking

                    //System.out.println("");

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }
            return null;
        }
    }
}
